package gherkin.stepdefinition.prestadores;

import br.com.pom.prestadores.pages.cadastro.Cadastro;
import cucumber.api.PendingException;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import io.cucumber.datatable.DataTable;

public class CadastroSteps {

    private Cadastro cadastro = new Cadastro();

    @E("^selecionar o codigo do prestador$")
    public void selecionarOCodigoDoPrestador(DataTable dataTable) throws Throwable {
        cadastro.validarTelaIncluirPerfilPrestador();
        cadastro.selecionarPrestador(dataTable);
    }

    @E("^preenche dados do perfil prestador$")
    public void preencheDadosDoPerfilPrestador(DataTable dataTable) throws Throwable {
        cadastro.crudPrestador(dataTable);
    }

    @E("^seleciona um primeiro item da tabela$")
    public void selecionaUmItemDaTabela() throws Throwable {
        cadastro.selecionaRegistro();
    }

    @E("^seleciona ultimo item da tabela$")
    public void selecionaUltimoItemDaTabela() throws Throwable {
        cadastro.selecionaUltimoRegistro();
    }

    @E("^seleciona item valido$")
    public void selecionaItemValido() throws Throwable {
        cadastro.selecionaItemValido();
    }

    @Entao("^eu consulto para validar$")
    public void euConsultoParaValidar() throws Throwable {
        cadastro.validaDadosDoPrestadorIncluido();
    }

    @Entao("^eu valido se a tela perfil prestador foi apresetada$")
    public void euValidoSeATelaPerfilPrestadorFoiApresetada() {
        cadastro.validaExclusaoPrestador();
    }
}
