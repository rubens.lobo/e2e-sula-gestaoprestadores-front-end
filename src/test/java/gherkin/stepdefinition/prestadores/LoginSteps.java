package gherkin.stepdefinition.prestadores;

import br.com.pom.prestadores.pages.Login.Login;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Quando;
import io.cucumber.datatable.DataTable;

public class LoginSteps {

    private Login login = new Login();

    @Dado("^eu estou na tela de login$")
    public void euEstouNaTelaDeLogin() {
        login.acessarAplicacao();
        login.validarTelaLogin();
    }

    @Quando("^eu logar com as credenciais$")
    public void euLogarComAsCredenciais(DataTable dataTable) {
        login.logar(dataTable);
    }



}
