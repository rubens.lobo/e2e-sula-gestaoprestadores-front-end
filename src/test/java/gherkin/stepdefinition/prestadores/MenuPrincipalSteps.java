package gherkin.stepdefinition.prestadores;

import br.com.pom.prestadores.pages.menuprincipal.MenuPrincipal;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class MenuPrincipalSteps {

    private MenuPrincipal menuPrincipal = new MenuPrincipal();

    @Entao("^sera apresentado o menu principal$")
    public void seraApresentadoOMenuPrincipal() {
        menuPrincipal.validaTelaMenuPrincipal();
    }

    @Quando("^eu acessar o menu \"([^\"]*)\"$")
    public void euAcessarOMenu(String menu) throws Throwable {
        menuPrincipal.selecionarMenu(menu);
    }

    @E("^clicar no item \"([^\"]*)\"$")
    public void clicarNoItem(String item) throws Throwable {
        menuPrincipal.itemMenu(item);
    }
}
