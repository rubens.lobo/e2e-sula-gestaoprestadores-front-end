package gherkin.stepdefinition.prestadores;

import br.com.pom.prestadores.pages.consulta.Consulta;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import io.cucumber.datatable.DataTable;

public class ConsultaSteps {

    private Consulta consulta =  new Consulta();

    @E("^selecionar um prestador$")
    public void selecionarUmPrestador(DataTable dataTable) throws Throwable {
        consulta.preencherFitroConsulta(dataTable);
        consulta.clicarNoBotaoPesquisar();
    }

    @Entao("^sera apresentado todos detalhes do prestador$")
    public void seraApresentadoTodosDetalhesDoPrestador() throws Throwable {
        consulta.validaTelaResumoDasdosPrestasdor();
    }
}
