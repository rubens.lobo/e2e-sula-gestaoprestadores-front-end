package br.com.pom.prestadores.model;

public class Prestador {

    private static String codPrestador;
    private static String descPrestador;
    private static String codRede;
    private static String codPlano;
    private static String tipoAtendimento;
    private static String descUS;
    private static String codNegociacao;
    private static String codAnestesico;
    private static String codFilme;
    private static String codReajuste;
    private static String dtInicioVigencia;
    private static String codFatorGerarAjuste;

    public static String getCodPrestador() {
        return codPrestador;
    }

    public static void setCodPrestador(String codPrestador) {
        Prestador.codPrestador = codPrestador;
    }

    public static String getDescPrestador() {
        return descPrestador;
    }

    public static void setDescPrestador(String descPrestador) {
        Prestador.descPrestador = descPrestador;
    }

    public static String getCodRede() {
        return codRede;
    }

    public static void setCodRede(String codRede) {
        Prestador.codRede = codRede;
    }

    public static String getTipoAtendimento() {
        return tipoAtendimento;
    }

    public static void setTipoAtendimento(String tipoAtendimento) {
        Prestador.tipoAtendimento = tipoAtendimento;
    }

    public static String getDescUS() {
        return descUS;
    }

    public static void setDescUS(String descUS) {
        Prestador.descUS = descUS;
    }

    public static String getCodNegociacao() {
        return codNegociacao;
    }

    public static void setCodNegociacao(String codNegociacao) {
        Prestador.codNegociacao = codNegociacao;
    }

    public static String getCodAnestesico() {
        return codAnestesico;
    }

    public static void setCodAnestesico(String codAnestesico) {
        Prestador.codAnestesico = codAnestesico;
    }

    public static String getCodFilme() {
        return codFilme;
    }

    public static void setCodFilme(String codFilme) {
        Prestador.codFilme = codFilme;
    }

    public static String getCodReajuste() {
        return codReajuste;
    }

    public static void setCodReajuste(String codReajuste) {
        Prestador.codReajuste = codReajuste;
    }

    public static String getDtInicioVigencia() {
        return dtInicioVigencia;
    }

    public static void setDtInicioVigencia(String dtInicioVigencia) {
        Prestador.dtInicioVigencia = dtInicioVigencia;
    }

    public static String getCodFatorGerarAjuste() {
        return codFatorGerarAjuste;
    }

    public static void setCodFatorGerarAjuste(String codFatorGerarAjuste) {
        Prestador.codFatorGerarAjuste = codFatorGerarAjuste;
    }

    public static String getCodPlano() {
        return codPlano;
    }

    public static void setCodPlano(String codPlano) {
        Prestador.codPlano = codPlano;
    }
}
