package br.com.pom.prestadores.pages.consulta;

import br.com.core.asserts.Verifications;
import br.com.core.report.ExtentReports;
import br.com.core.setup.DriverManager;
import br.com.core.view.Action;
import br.com.core.view.ComboBoxActions;
import br.com.pom.prestadores.constantes.Constantes;
import br.com.pom.prestadores.model.Prestador;
import io.cucumber.datatable.DataTable;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

public class Consulta extends DriverManager implements Constantes {

    private By txtCodPrestador = By.name("codPrestador");
    private By btnSelPrestador = By.xpath("//input[contains(@onclick,'prestador') and  @value='Selecionar']");
    private By cbbListaAtendimento = By.name("codTipAtend");
    private By txtCodTabelaNegociacao = By.name("codTabnegNovo");
    private By btnTabelaNegociacao = By.xpath("//input[contains(@onclick,'negociacao') and  @value='Selecionar']");
    private By txtCodTabreajuste = By.name("codTabreajNovo");
    private By btnCodTabreajuste = By.xpath("//input[contains(@onclick,'reajuste') and  @value='Selecionar']");
    private By btnPesquisar = By.xpath("//input[@name='botao.consultar' and @value='Selecionar' or @value='Pesquisar']");

    /**
     * Execeuta pesquisa de acordo com o filtro
     *
     * @param dataTable - envia os dados para filtrar a busca
     */
    public void preencherFitroConsulta(DataTable dataTable){
        String rede = null;
        String plano;
        List<Map<String,String>> table = dataTable.asMaps();

        for (Map<String,String> row : table){
            for(String campo : row.keySet()){
                String resposta = row.get(campo);
                switch (campo.toUpperCase()){
                    case "COD PRESTADOR":
                        Action.setText(getBrowser(),txtCodPrestador,resposta,TIME_OUT);
                        Action.clickOnElement(getBrowser(),btnSelPrestador,TIME_OUT);
                        break;
                    case "REDE":
                        rede = resposta;
                        Prestador.setCodRede(resposta);
                        break;
                    case "PLANO":
                        plano = resposta;
                        Action.clickOnElement(getBrowser(),By.xpath("//select[@name='codRedePlano']//optgroup[@label='"
                                +rede+"']//option[contains(text(),'"+plano+"')]"),TIME_OUT);
                        break;
                    case "TIPO-ATENDIMENTO":
                        ComboBoxActions.selectComboOptionByText(getBrowser(),cbbListaAtendimento,resposta,TIME_OUT);
                        break;
                    case "TABELA-NEGOCIACAO":
                        Action.setText(getBrowser(),txtCodTabelaNegociacao,resposta,TIME_OUT);
                        Action.clickOnElement(getBrowser(),btnTabelaNegociacao,TIME_OUT);
                        break;
                    case "TABELA-REAJUSTE":
                        Action.setText(getBrowser(),txtCodTabreajuste,resposta,TIME_OUT);
                        Action.clickOnElement(getBrowser(),btnCodTabreajuste,TIME_OUT);
                        break;
                    default:
                        Assert.fail("Coluna não encontrada!");
                }
            }
        }
    }

    /**
     *  Clica no botão selecionar
     *
     */
    public void clicarNoBotaoPesquisar(){
        Verifications.verifyElementIsClickable(getBrowser(),btnPesquisar,TIME_OUT);
        ExtentReports.appendToReport(getBrowser());
        getBrowser().findElements(btnPesquisar).get(3).click();
    }

    /**
     * Valida se a tela resumo das informações do prestador foi apresentada
     *
     */
    public void validaTelaResumoDasdosPrestasdor(){
        Verifications.verifyElementExists(getBrowser(),By.name("frmPrestadorPerfil"),TIME_OUT);
        ExtentReports.appendToReport(getBrowser());
    }
}
