package br.com.pom.prestadores.pages.Login;

import br.com.core.asserts.Verifications;
import br.com.core.report.ExtentReports;
import br.com.core.setup.AppWeb;
import br.com.core.setup.DriverManager;
import br.com.core.view.Action;
import br.com.core.view.ComboBoxActions;
import br.com.pom.prestadores.constantes.Constantes;
import io.cucumber.datatable.DataTable;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

public class Login  extends DriverManager implements Constantes{

    private By txtLogin = By.id("usuario");
    private By txtPass = By.id("senha");
    private By txtModulo = By.name("modulo");
    private By btnAcessar = By.name("login");

    /**
     * Abri navegador
     */
    public void acessarAplicacao(){
        AppWeb appWeb = new AppWeb();
        appWeb.setTestName(testScenario.get().getName());
        appWeb.setBrowserName("Chrome");
        appWeb.setUpDriver(appWeb);
        getBrowser().get(URL);
    }

    /**
     * Valida se tela de login foi apresentada
     */
    public void validarTelaLogin(){
        Verifications.verifyElementIsClickable(getBrowser(),txtLogin,TIME_OUT);
        ExtentReports.appendToReport(getBrowser());
    }

    /**
     * Executa login na aplicação
     *
     * @param dataTable - Tabela com dados de usuario, senha e modulo
     */
    public void logar(DataTable dataTable){
        List<Map<String,String>> table = dataTable.asMaps();

        for (Map<String,String> row : table){
            for(String campo : row.keySet()){
                String resposta = row.get(campo);
                switch (campo.toUpperCase()){
                    case "USUARIO":
                        Action.setText(getBrowser(),txtLogin,resposta,TIME_OUT);
                        break;
                    case "SENHA":
                        Action.setText(getBrowser(),txtPass,resposta,TIME_OUT);
                        break;
                    case "MODULO":
                        ComboBoxActions.selectComboOptionByText(getBrowser(),txtModulo,resposta,TIME_OUT);
                        break;
                    default:
                        Assert.fail("Coluna não encontrada!");
                }
            }
        }

        Action.clickOnElement(getBrowser(),btnAcessar,TIME_OUT);

    }


}
