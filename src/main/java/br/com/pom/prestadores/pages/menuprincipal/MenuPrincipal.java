package br.com.pom.prestadores.pages.menuprincipal;

import br.com.core.asserts.Verifications;
import br.com.core.report.ExtentReports;
import br.com.core.setup.DriverManager;
import br.com.core.view.Action;
import br.com.pom.prestadores.constantes.Constantes;
import org.openqa.selenium.By;
import org.testng.Assert;

public class MenuPrincipal extends DriverManager implements Constantes{

    private By lnkSair = By.linkText("Sair");

    /**
     * Valida acesso ao menu principal
     *
     */
    public void validaTelaMenuPrincipal(){
        Verifications.verifyElementIsClickable(getBrowser(),lnkSair,TIME_OUT);
        ExtentReports.appendToReport(getBrowser());
    }

    /**
     *
     *  Seleciona item no menu
     *
     * @param menu - Menu principal
     */
    public void selecionarMenu(String menu){
        getBrowser().findElements(By.xpath("//ul[@id='abas']//li/a[text()='"+menu +"']")).get(0).click();
    }

    /**
     *
     *  Seleciona item internamente no menu
     *
     * @param item - item interno ao menu
     */
    public void itemMenu(String item){
        switch (item.toUpperCase()){
            case "INCLUIR":
                getBrowser().findElements(By.xpath("//ul[@id='abas']//li/a[text()='Cadastros']/../ul//li/a")).get(0).click();
                break;
            case "ALTERAR":
                getBrowser().findElements(By.xpath("//ul[@id='abas']//li/a[text()='Cadastros']/../ul//li/a")).get(1).click();
                break;
            case "ALTERAR HISTÓRICO":
                getBrowser().findElements(By.xpath("//ul[@id='abas']//li/a[text()='Cadastros']/../ul//li/a")).get(2).click();
                break;
            case "EXCLUIR":
                getBrowser().findElements(By.xpath("//ul[@id='abas']//li/a[text()='Cadastros']/../ul//li/a")).get(3).click();
                break;
            case "PERFIL PRESTADOR":
                Action.clickOnElement(getBrowser(),By.xpath("//ul[@id='sub0']//li/a"),TIME_OUT);
                break;
            case "RETIFICAR":
                Action.clickOnElement(getBrowser(),By.xpath("//ul[@id='abas']//li/a[text()='Tarefas']/../ul//li/a[text()='Retificar']"),TIME_OUT);
                break;
            default:
                Assert.fail("Coluna não encontrada!");
        }
    }
}
