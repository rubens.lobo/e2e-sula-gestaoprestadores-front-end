package br.com.pom.prestadores.pages.cadastro;

import br.com.core.asserts.Verifications;
import br.com.core.dates.DatePicker;
import br.com.core.report.ExtentReports;
import br.com.core.setup.DriverManager;
import br.com.core.view.Action;
import br.com.core.view.ComboBoxActions;
import br.com.pom.prestadores.constantes.Constantes;
import br.com.pom.prestadores.model.Prestador;
import io.cucumber.datatable.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import java.util.List;
import java.util.Map;

public class Cadastro extends DriverManager implements Constantes{

    private By frmPrestador = By.name("frmPrestadorPerfil");
    private By rowTable = By.xpath("//input[@name='codigo']");
    private By txtCodPrestador = By.name("codPrestador");
    private By btnSelecionar = By.name("botao.selecionar");
    private By cbbListaAtendimento = By.name("lstTipAtendSel");
    private By cbbTabelaUs = By.name("codTabus");
    private By txtCodTabelaNegociacao = By.name("codTabnegNovo");
    private By btnTabelaNegociacao = By.xpath("//input[contains(@onclick,'negociacao')]");
    private By txtCodTabporteAnestesico = By.name("codTabporteNovo");
    private By btnCodTabporteAnestesico = By.xpath("//input[contains(@onclick,'porte')]");
    private By txtCodTabfilme = By.name("codTabfilmeNovo");
    private By btnCodTabfilme = By.xpath("//input[contains(@onclick,'filme')]");
    private By txtCodTabreajuste = By.name("codTabreajNovo");
    private By btnCodTabreajuste = By.xpath("//input[contains(@onclick,'reajuste')]");
    private By txtInicioVigencia = By.name("datInivigencia");
    private By txtFimVigencia = By.name("datFimvigencia");
    private By txtFatorFilme = By.name("fatFilme");
    private By txtFatorGeralReajuste = By.name("fatReajuste");
    private By btnGravar = By.xpath("//input[contains(@onclick,'enviar')]");
    private By btnPesquisar = By.xpath("//input[contains(@onclick,'pesquisar')]");
    private By tbDadosPrestador = By.xpath("//tbody");

    /**
     *  valida se a tela Incluir Perfil Prestador foi apresentada
     */
    public void validarTelaIncluirPerfilPrestador(){
        Verifications.verifyElementIsClickable(getBrowser(),txtCodPrestador,TIME_OUT);
        ExtentReports.appendToReport(getBrowser());
    }

    /**
     *  Seleciona um prestador na tela Incluir Perfil Prestador na tela de pesquisa
     *
     * @param dataTable
     */
    public void selecionarPrestador(DataTable dataTable){
        List<Map<String,String>> table = dataTable.asMaps();

        for (Map<String,String> row : table){
            for(String campo : row.keySet()){
                String resposta = row.get(campo);
                switch (campo.toUpperCase()){
                    case "COD PRESTADOR":
                        getBrowser().findElement(txtCodPrestador).sendKeys(resposta);
                        break;
                    case "ACAO":
                        if(resposta.equalsIgnoreCase("Alterar") || resposta.equalsIgnoreCase("Excluir")
                                || resposta.equalsIgnoreCase("Consultar")){
                            Action.clickOnElement(getBrowser(),btnPesquisar,TIME_OUT);
                        }else if(resposta.equalsIgnoreCase("Incluir")){
                            Action.clickOnElement(getBrowser(),btnSelecionar,TIME_OUT);
                        }
                        break;
                    default:
                        Assert.fail("Coluna não encontrada!");
                }
            }
        }
    }

    /**
     * Este metodo pode incluir, alterar, alterar historico e excluir prestador
     *
     * @param dataTable - Envie os dados que deseja alterar ou criar
     */
    public void crudPrestador(DataTable dataTable){

        String plano;
        String rede = null;

        List<Map<String,String>> table = dataTable.asMaps();


        for (Map<String,String> row : table){
            for(String campo : row.keySet()){
                String resposta = row.get(campo);
                switch (campo.toUpperCase()){
                    case "REDE":
                        rede = resposta;
                        Prestador.setCodRede(resposta);
                        break;
                    case "PLANO":
                        plano = resposta;
                        Action.clickOnElement(getBrowser(),By.xpath("//select[@name='lstRedePlanoSel']//optgroup[@label='"
                                +rede+"']//option[contains(text(),'"+plano+"')]"),TIME_OUT);
                        Prestador.setCodPlano(plano);
                        break;
                    case "TIPO-ATENDIMENTO":
                        ComboBoxActions.selectComboOptionByText(getBrowser(),cbbListaAtendimento,resposta,TIME_OUT);
                        Prestador.setTipoAtendimento(resposta);
                        break;
                    case "TABELA-US":
                        ComboBoxActions.selectComboOptionByText(getBrowser(),cbbTabelaUs,resposta,TIME_OUT);
                        Prestador.setDescUS(resposta);
                        break;
                    case "TABELA-NEGOCIACAO":
                        Action.setText(getBrowser(),txtCodTabelaNegociacao,resposta,TIME_OUT);
                        Action.clickOnElement(getBrowser(),btnTabelaNegociacao,TIME_OUT);
                        Prestador.setCodNegociacao(resposta);
                        break;
                    case "PORTE-ANESTESICO":
                        Action.setText(getBrowser(),txtCodTabporteAnestesico,resposta,TIME_OUT);
                        Action.clickOnElement(getBrowser(),btnCodTabporteAnestesico,TIME_OUT);
                        Prestador.setCodAnestesico(resposta);
                        break;
                    case "TABELA-FILME":
                        Action.setText(getBrowser(),txtCodTabfilme,resposta,TIME_OUT);
                        Action.clickOnElement(getBrowser(),btnCodTabfilme,TIME_OUT);
                        Prestador.setCodFilme(resposta);
                        break;
                    case "TABELA-REAJUSTE":
                        Action.setText(getBrowser(),txtCodTabreajuste,resposta,TIME_OUT);
                        Action.clickOnElement(getBrowser(),btnCodTabreajuste,TIME_OUT);
                        Prestador.setCodReajuste(resposta);
                        break;
                    case "INICIO-VIGENCIA":
                        if(resposta.isEmpty()){
                            Action.setText(getBrowser(), txtInicioVigencia, DatePicker.getCurrentDate(),TIME_OUT);
                            Prestador.setDtInicioVigencia(resposta);
                        }else {
                            if(resposta.toUpperCase().contains("NOVA")){
                                if(!getBrowser().findElement(txtInicioVigencia).getAttribute("value").isEmpty()){
                                    String date = getBrowser().findElement(txtInicioVigencia).getAttribute("value");
                                    Action.setText(getBrowser(), txtInicioVigencia, DatePicker.getNextDay(date), TIME_OUT);
                                    Prestador.setDtInicioVigencia(resposta);
                                }else {
                                    Action.setText(getBrowser(), txtInicioVigencia, resposta, TIME_OUT);
                                    Prestador.setDtInicioVigencia(resposta);
                                }

                            }else {
                                Action.setText(getBrowser(), txtInicioVigencia, resposta, TIME_OUT);
                                Prestador.setDtInicioVigencia(resposta);
                            }
                        }
                        break;
                    case "FIM-VIGENCIA":
                        try{
                        if(resposta.isEmpty()){
                            if(!getBrowser().findElement(txtInicioVigencia).getAttribute("value").isEmpty()){
                                String date = getBrowser().findElement(txtInicioVigencia).getAttribute("value");
                                Action.setText(getBrowser(), txtFimVigencia, DatePicker.getNextDay(date),TIME_OUT);
                            }else {
                                Action.setText(getBrowser(), txtInicioVigencia, resposta, TIME_OUT);
                            }
                        }else {
                            Action.setText(getBrowser(), txtFimVigencia, resposta,TIME_OUT);
                        }}catch (Exception e){
                            ExtentReports.appendToReport("WARNING: " +e.getMessage());
                        }
                        break;
                    case "FATOR-FILME":
                        Action.setText(getBrowser(),txtFatorFilme,resposta,TIME_OUT);
                        Prestador.setCodFilme(resposta);
                        break;
                    case "FATOR-GERA-REAJUSTE":
                        Action.setText(getBrowser(),txtFatorGeralReajuste,resposta,TIME_OUT);
                        Prestador.setCodReajuste(resposta);
                        break;
                    default:
                        Assert.fail("Coluna não encontrada!");
                }
            }
        }

        ExtentReports.appendToReport(getBrowser());
        Action.clickOnElement(getBrowser(),btnGravar,TIME_OUT);


    }

    /**
     * Seleciona primeiro registro da tabela prestadores
     *
     */
    public void selecionaRegistro(){
        Verifications.verifyElementIsClickable(getBrowser(),rowTable,TIME_OUT);
        ExtentReports.appendToReport(getBrowser());
        Action.clickOnElement(getBrowser(),rowTable,TIME_OUT);
        Action.clickOnElement(getBrowser(),btnGravar,TIME_OUT);
    }

    /**
     * Seleciona ultimo registro da tabela prestadores
     *
     */
    public void selecionaUltimoRegistro(){
        Verifications.verifyElementIsClickable(getBrowser(),rowTable,TIME_OUT);
        getBrowser().findElements(rowTable).get(getBrowser().findElements(rowTable).size()-1).click();
        ExtentReports.appendToReport(getBrowser());
        List<WebElement> buttons = getBrowser().findElements(By.name("botao.consultar"));
        buttons.get(buttons.size()-1).click();
    }

    /**
     * Valida se as informações od prestador está na tela, conforme realizado na inclusão de novo prestador
     *
     */
    public void validaDadosDoPrestadorIncluido(){
        Verifications.verifyElementExists(getBrowser(),tbDadosPrestador,TIME_OUT);
        ExtentReports.appendToReport(getBrowser());
        WebElement table = getBrowser().findElement(tbDadosPrestador);
        table.getText().contains(Prestador.getCodPlano());
        table.getText().replace(" ","")
                .contains(Prestador.getCodRede().replace("Rede","").replace(" ",""));
        getBrowser().findElement(tbDadosPrestador).getText().contains(Prestador.getTipoAtendimento());
        getBrowser().findElement(tbDadosPrestador).getText().contains(Prestador.getCodAnestesico());
        getBrowser().findElement(tbDadosPrestador).getText().contains(Prestador.getCodNegociacao());
        getBrowser().findElement(tbDadosPrestador).getText().contains(Prestador.getCodReajuste());
        getBrowser().findElement(tbDadosPrestador).getText().contains(Prestador.getDtInicioVigencia());
    }

    /**
     * Valida se exclusão foi realizada com sucesso
     */
    public void validaExclusaoPrestador(){
        Verifications.verifyElementExists(getBrowser(),frmPrestador,TIME_OUT);
        ExtentReports.appendToReport(getBrowser());
    }

    /**
     * Seleciona item valido na tabela para alterar, excluir e alterar historico
     */
    public void selecionaItemValido(){

        Verifications.wait(1);
        List<WebElement> endDates = getBrowser().findElements(By.xpath("//table/tbody/tr/td[9]"));
        List<WebElement> radiobuttons = getBrowser().findElements(By.xpath("//table[2]/tbody/tr/td[1]/input"));

        for (int i = 0; i < endDates.size(); i++) {
            if(endDates.get(i).getText().contains("9999")){
                radiobuttons.get(i).click();
                break;
            }
        }
        ExtentReports.appendToReport(getBrowser());
        List<WebElement> buttons = getBrowser().findElements(By.name("botao.consultar"));
        buttons.get(buttons.size()-1).click();

    }

}
