# SISTEMA - PRESTADORES

Projeto desenvolvido para rodar rotinas automatizadas de testes

## PLANO DE TESTE

Link: https://docs.google.com/spreadsheets/d/1JRmOe5ztaaAp5mpgNkfGhWzhd68-p12-3ZENrC_m1bQ/edit?usp=sharing

## TESTES AUTOMATIZADOS

*   Cadastros
    * [CT001 - PR - FPOS-9 - Cadastro - Incluir perfil prestador](feature/Cadastro.feature)
    * [CT002 - PR - FPOS-9 - Cadastro - Alterar perfil prestador](feature/Cadastro.feature)
    * [CT003 - PR - FPOS-9 - Cadastro - Alterar Histórico do perfil prestador](feature/Cadastro.feature)
    * [CT004 - PR - FPOS-9 - Cadastro - Excluir perfil prestador](feature/Cadastro.feature)
*   Consultas
    * [CT001 - PR - FPOS-9 - Consultas - Consultar perfil prestador](feature/Consulta.feature)
*   Tarefas
    * [CT001 - PR - FPOS-9 - Tarefas - Retificar prestador ](feature/Tarefas.feature)


## PRÉ-REQUISITOS

Requisitos de software para executar este projeto de automação

*   Java 1.8 SDK
*   Maven 3.5.*
*   Chrome ou Firefox qualquer versão.

## CLONE O PROJETO PARA SUA MÁQUINA LOCAL

Abra o git bash, entre no diretório escolhido na sua máquina e faça o download do projeto com o comando abaixo.

```
git clone https://gitlab.com/rubens.lobo/e2e-sula-gestaoprestadores-front-end.git
```

## COMANDO PARA EXECUTAR OS TESTES

Com o git bash acesse a pasta do projeto, onde esta localizado o arquivo pom.xml, execute o comando abaixo para rodar os testes automatizados.

Comando básico, por default o chrome é utilizado na execução

```
mvn verify
```

Você pode executar seus testes em diferentes navegadores, você pode passar este parâmetro via linha de comando maven, como apresentado abaixo;

* Chrome 
~~~~
mvn verify -Dbrowser=Chrome -Durl=https://www.web-site.com.br 
~~~~

* Firefox 
~~~~
mvn verify -Dbrowser=Firefox -Durl=https://www.web-site.com.br 
~~~~

* Navegador Chrome HeadLess 
~~~~
mvn verify -Dbrowser=HeadLess -Durl=https://www.web-site.com.br 
~~~~

Você também pode mesclar linha de comando maven com options do cucumber, sendo assim você pode escolher uma determinada tag que se deseja executar do cucumber.

* Cucumber 

```
mvn verify -Dbrowser=Chrome -Durl=https://www.web-site.com.br -Dcucumber.options="--tags @runThis --tags ~@ignore"
```

Nesta linha de comando acima, nós iremos executar os testes no navegador Chrome que contenha a tag @runThis e será ignorado todos os testes que contenha a tag @ignore.

## VIDEO TUTORIAL

Video tutorial sobre estrutura, execução e evidências.

[![Watch the video](src/test/resources/imagens/video.jpg)](https://drive.google.com/open?id=18wxOgz99E2IP2CN4v5D3ZysMI4NSb-_r)

## EVIDÊNCIAS

Os arquivos com as evidências ficam localizados na pasta target do projeto, esta pasta só é criada depois da primeira execução.

```
 Report HTML: target\generated-report\index.html
 Json Cucumber: target\json-cucumber-reports\cukejson.json
 Xml TestNG: target\testng-cucumber-reports\cuketestng.xml
```
<div align="center">
    <img id="header" src="./src/test/resources/imagens/evidencia.jpg" />
</div>

## LOG NO CONSOLE

Veja os resultados do testes no console com status e também a localização dos arquivos mencionados acima.

<div align="center">
    <img id="header" src="./src/test/resources/imagens/resultado.jpg" />
</div>

## AUTOR

* **Rubens Lobo**