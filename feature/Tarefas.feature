# language: pt
# charset: UTF-8

@Tarefas
Funcionalidade: Tarefas
  Eu como usuário da aplicação gostaria retificar as tarefas do prestador

  Contexto:
    Dado eu estou na tela de login
    Quando eu logar com as credenciais
      |USUARIO              |SENHA    |MODULO     |
      |homolgp001           |111111   |Negociação |
    Entao sera apresentado o menu principal

  @Retificar
  Cenario: CT001 - PR - FPOS-9 - Tarefas - Retificar prestador
    Quando eu acessar o menu "Tarefas"
    E clicar no item "Retificar"
    E selecionar um prestador
      |COD PRESTADOR|TABELA-NEGOCIACAO|TABELA-REAJUSTE|
      |100000015194 |472377           |147497         |
    E seleciona item valido
    E preenche dados do perfil prestador
      |FATOR-GERA-REAJUSTE|
      |3                  |
    Entao eu valido se a tela perfil prestador foi apresetada