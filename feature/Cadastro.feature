# language: pt
# charset: UTF-8

@Cadastro
Funcionalidade: Cadastro
  Eu como usuário da aplicação gostaria de aplicar
  o tipo de remuneração do prestador para referência.

  Contexto:
    Dado eu estou na tela de login
    Quando eu logar com as credenciais
      |USUARIO              |SENHA    |MODULO     |
      |homolgp001           |111111   |Negociação |
    Entao sera apresentado o menu principal

#  TIPO-ATENDIMENTO - Tipos de atendimento que pode ser utilizado
#  PRONTO SOCORRO
#  DAY HOSPITAL
#  INTERNADO - ENFERMARIA
#  INTERNADO - APARTAMENTO
#  AMBULATORIO

  @Incluir
  Cenario: CT001 - PR - FPOS-9 - Cadastro - Incluir perfil prestador
    Quando eu acessar o menu "Cadastros"
    E clicar no item "Incluir"
    E selecionar o codigo do prestador
      |COD PRESTADOR|ACAO   |
      |100000015194 |Incluir|
    E preenche dados do perfil prestador
      |REDE                                |PLANO      |TIPO-ATENDIMENTO       |TABELA-US|TABELA-NEGOCIACAO|PORTE-ANESTESICO|TABELA-FILME|TABELA-REAJUSTE|INICIO-VIGENCIA|FATOR-FILME|FATOR-GERA-REAJUSTE|
      |Rede 0000003014-SAUDE TRAD. 10 ADM  |EXECUTIVO  |AMBULATORIO            |REAL     |472377           |11              |1           |147497         |               |1          |1                  |
    E selecionar o codigo do prestador
      |ACAO     |
      |Consultar|
    E seleciona ultimo item da tabela
    Entao eu consulto para validar

  @Alterar @dev
  Cenario: CT002 - PR - FPOS-9 - Cadastro - Alterar perfil prestador
    Quando eu acessar o menu "Cadastros"
    E clicar no item "Alterar"
    E selecionar um prestador
      |COD PRESTADOR|
      |100000015194 |
    E seleciona item valido
    E preenche dados do perfil prestador
      |FATOR-GERA-REAJUSTE|INICIO-VIGENCIA|
      |3                  |Nova           |
    Entao eu valido se a tela perfil prestador foi apresetada

  @AlterarHistorico
  Cenario: CT003 - PR - FPOS-9 - Cadastro - Alterar Histórico do perfil prestador
    Quando eu acessar o menu "Cadastros"
    E clicar no item "Alterar Histórico"
    E selecionar um prestador
      |COD PRESTADOR|
      |100000015194 |
    E seleciona item valido
    E preenche dados do perfil prestador
      |FATOR-GERA-REAJUSTE|INICIO-VIGENCIA|
      |3                  |Nova           |
    Entao eu valido se a tela perfil prestador foi apresetada

  @Excluir
  Cenario: CT004 - PR - FPOS-9 - Cadastro - Excluir perfil prestador
    Quando eu acessar o menu "Cadastros"
    E clicar no item "Excluir"
    E selecionar um prestador
      |COD PRESTADOR|
      |100000015194 |
    E seleciona item valido
    E preenche dados do perfil prestador
      |FIM-VIGENCIA|
      |            |
    Entao eu valido se a tela perfil prestador foi apresetada