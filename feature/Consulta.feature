# language: pt
# charset: UTF-8

@Consulta
Funcionalidade: Consulta
  Eu como usuário da aplicação gostaria de realizar uma consulta
  sobre os dados dos clientes

  Contexto:
    Dado eu estou na tela de login
    Quando eu logar com as credenciais
      |USUARIO              |SENHA    |MODULO     |
      |homolgp001           |111111   |Negociação |
    Entao sera apresentado o menu principal

  @Consulta
  Cenario: CT001 - PR - FPOS-9 - Consultas - Consultar perfil prestador
    Quando eu acessar o menu "Consultas"
    E clicar no item "Perfil Prestador"
    E selecionar um prestador
      |COD PRESTADOR|REDE                                           |PLANO      |TIPO-ATENDIMENTO|TABELA-NEGOCIACAO|TABELA-REAJUSTE|
      |100000015194 |Rede 0000000102-SAS CONVENCIONAL ADMINISTRADO  |BASICO     |DAY HOSPITAL    |472377           |147497         |
    E seleciona ultimo item da tabela
    Entao sera apresentado todos detalhes do prestador